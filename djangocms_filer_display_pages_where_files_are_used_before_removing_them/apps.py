from django.apps import AppConfig


class DjangocmsFilerDisplayPagesWhereFilesAreUsedBeforeRemovingThemConfig(AppConfig):
    name = "djangocms_filer_display_pages_where_files_are_used_before_removing_them"
